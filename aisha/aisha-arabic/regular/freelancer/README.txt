# Aisha

Designer: Titus Nemeth
Released by: Rosetta Type Foundry
Website: http://rosettatype.com/Aisha

## This package contains

- font(s) in OTF and/or TTF format
- Rosetta EULA

## Notes

Do not install both OTF and TTF fonts at the same time.

PDF type specimen with description of all OpenType features can be downloaded from the website. The website also includes downloadable character set overviews and a list of supported languages.

Should you have any issues with our font(s), do not hesitate to contact us at <info@rosettatype.com>.
