
# Skolar Sans

Designer: David Březina & Sláva Jevčinová
Released by: Rosetta Type Foundry
Website: http://rosettatype.com/SkolarSans


## This package contains

- font(s) in OTF and/or TTF format
- Rosetta EULA
- PDF specimen


## Desktop fonts

Do not install both OTF and TTF fonts at the same time!

PDF type specimen with description of all OpenType features can be downloaded from the website. The website also includes downloadable character set overviews and a list of supported languages.

Also, to get the best out of the fonts, we recommend to keep the *contextual alternates* on at all times if the application supports them.


## Web fonts

WOFF, WOFF2, EOT, and TTF fonts are provided. However, the TTF fonts are intended only for creating mock-ups. They should not be installed on your web server! HTML file contains an example of how to use the webfonts. Relevant CSS is included in the file as well.


## Contact

Should you have any issues with our font(s), do not hesitate to contact us at <info@rosettatype.com>.

