#!/usr/bin/env python
# encoding: utf-8
"""
copyFontsFromRepo.py
"""

import os, sys, optparse, glob
from shutil import copyfile
from pipes import quote

__version__ = "1.00"
__copyright__ = "Copyright (c) 2015 by David Brezina"
__description__ = """Copies fonts from the Rosetta repo to specified folder."""
__requirements__ = "Requires Python 2.5 or higher"

LOGNAME = "copyFontsFromRepo"

def warn(message, category = "WARN", exit = False):
	sys.stderr.write("[%s][%s] %s\n" % (LOGNAME, category, message))
	sys.stderr.flush()
	if exit: 
		sys.exit(2)

def parseOptions():
	usage = "usage: %prog <output path>"
	parser = optparse.OptionParser(formatter=optparse.TitledHelpFormatter(), usage=usage, version=__version__, description="%prog " + "version %s. %s. %s" % (__version__, __copyright__, __description__), epilog=__requirements__)
	
	parser.add_option("-v", "--verbose",
		action="store_true", dest="verbose", default=False,
		help="print additional information")
	parser.add_option("-t", "--type-families",
		action="store", dest="typefamilies", type="string", default="*",
		help="filter type families to copy, separated by comma, e.g. -f Arek,Aisha")
	parser.add_option("-s", "--subfamilies",
		action="store", dest="subfamilies", type="string", default="*",
		help="filter subfamilies to copy, separated by comma, e.g. -f Arabic,Latin")
	parser.add_option("-f", "--formats",
		action="store", dest="formats", type="string", default="otf",
		help="specify font formats to copy, separated by comma, e.g. -f ttf,otf,woff,woff2,eot,test")
	parser.add_option("-r", "--repo",
		action="store", dest="repopath", type="string", default="",
		help="path to Rosetta repo with fonts, use in case it is not in the current directory")
	return parser.parse_args()

def getAllTypeFamilies(repopath):
	paths = []
	for path in glob.glob(os.path.join(repopath, "*")):
		if os.path.isdir(path):
			paths.append(os.path.basename(path))
	return paths

def main():
	(options, args) = parseOptions()

	# check paths
	if len(args) > 0:
		if os.path.exists(args[0]):
			outpath = args[0]
		else:
			warn("Output path does not exists.", "ERRR", exit = True)
	else:
		warn("Use -h for help", "ERRR", exit = True)

	if options.repopath:
		if os.path.exists(options.repopath):
			repopath = options.repopath
		else:
			warn("Repo path does not exist.", exit = True)
	else:
		repopath = os.getcwd()

	# main business	
	typefamilies = options.typefamilies.split(",")
	if "*" in typefamilies:
		typefamilies = getAllTypeFamilies(repopath)
	subfamilies = options.subfamilies.split(",")
	if "*" in subfamilies:
		subfamilies = ["*"]
	# copy all styles
	style = "*"
	formats = options.formats.split(",")
	fonts = []

	for typefamily in typefamilies:
		typefamily = typefamily.lower()
		for subfamily in subfamilies:
			subfamily = "*"+subfamily.lower()+"*"
			for ext in formats:
				if ext in ["woff", "woff2", "eot"]:
					for font in glob.glob(os.path.join(repopath, typefamily, subfamily, style, "web", '*.'+ext)):
						fonts.append((font, os.path.join(typefamily, os.path.basename(font))))
				elif ext in ["ttf", "otf"]:
					for font in glob.glob(os.path.join(repopath, typefamily, subfamily, style, "studio", '*.'+ext)):
						fonts.append((font, os.path.join(typefamily, os.path.basename(font))))
				elif ext == "test":
					for font in glob.glob(os.path.join(repopath, typefamily, subfamily, style, "test", '*.otf')):
						fonts.append((font, os.path.join(typefamily, os.path.basename(font))))

	for font, targetfont in fonts:
		if os.path.exists(font):
			targetpath = os.path.join(outpath, targetfont)
			targetfolder = os.path.dirname(targetpath)
			if not os.path.exists(targetfolder):
				os.makedirs(targetfolder)
			copyfile(font, targetpath)
			print "Copying: %s to %s" % (os.path.basename(font), os.path.dirname(targetpath))
		else:
			warn("Could not find any fonts to copy. Is this correct source directory?")
if __name__ == "__main__":
	sys.exit(main())
