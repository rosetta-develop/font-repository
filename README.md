# Readme

Fonts for the Rosetta e-shop, testers, and continuouupdates. Sync it via admin on the Rosetta website.

Based on the general agreement regarding mutual sharing of fonts, designers now have access to this repo. These fonts are only for your inspection, not for actual use in design. Design courtesy is assumed. In case of doubt, just ask. Bug reports are welcome.

To access the fonts conveniently, just clone this repo and use the script `copyFontsFromRepo.py`. To get help, in Terminal, navigate to the root folder of the repo and type this command:

```
./copyFontsFromRepo.py -h
```

In case the script has insufficient rights, set them using `chmod +x copyFontsFromRepo.py`.

Here is an example that copies all Latin fonts of Aisha into `~/Library/Fonts` as OTFs to install them:

```
./copyFontsFromRepo.py -t Aisha -s Latin -f otf ~/Library/Fonts
```

The `./` part is important. It allows the script to run from the folder where it is saved.


## Technical note (designers ignore)

deployment key needs to be set properly.